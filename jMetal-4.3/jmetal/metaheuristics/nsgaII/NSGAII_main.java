//  NSGAII_main.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.metaheuristics.nsgaII;



import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.* ;

import jmetal.core.*;
import jmetal.operators.crossover.*;
import jmetal.operators.mutation.*;
import jmetal.operators.selection.*;
import jmetal.problems.*                  ;
import jmetal.problems.DTLZ.*;
import jmetal.problems.ZDT.*;
import jmetal.problems.WFG.*;
import jmetal.problems.LZ09.* ;

import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.MersenneTwisterFast;
import jmetal.util.PseudoRandom;
import jmetal.util.Ranking;

import java.io.IOException;
import java.util.* ;

import java.util.logging.FileHandler;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import jmetal.qualityIndicator.QualityIndicator;

/** 
 * Class to configure and execute the NSGA-II algorithm.  
 *     
 * Besides the classic NSGA-II, a steady-state version (ssNSGAII) is also
 * included (See: J.J. Durillo, A.J. Nebro, F. Luna and E. Alba 
 *                  "On the Effect of the Steady-State Selection Scheme in 
 *                  Multi-Objective Genetic Algorithms"
 *                  5th International Conference, EMO 2009, pp: 183-197. 
 *                  April 2009)
 */ 

public class NSGAII_main {
  public static Logger      logger_ ;      // Logger object
  public static FileHandler fileHandler_ ; // FileHandler object
  
  static int replaceNumber = 5000, evaluationNumber = (int)25000/(replaceNumber+2);
  
  static int numData = (int)((evaluationNumber*(replaceNumber+2))/200);

  
  public static double y[] = new double [numData];
  public static int indexY = 0;
  public static double y2[] = new double [numData];
  public static int indexY2 = 0;
  
  public static double y_2[] = new double [numData];
  public static int indexY_2 = 0;
  public static double y2_2[] = new double [numData];
  public static int indexY2_2 = 0;
  
  public static double yTest[] = new double [numData];
  public static int indexYTest = 0;
  public static double y2Test[] = new double [numData];
  public static int indexY2Test = 0;
  
  /**
   * @param args Command line arguments.
   * @throws JMException 
   * @throws IOException 
   * @throws SecurityException 
   * Usage: three options
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main problemName
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main problemName paretoFrontFile
 * @throws InterruptedException 
   */
  
  public static String[][] readData(int brojDatoteka, String imeDatoteke){
	  Scanner scanner = null;
	  String[] line = new String [brojDatoteka];
	  String[][] metrics = new String[brojDatoteka][49];
	  int l =0;
	  
	  try {
  		//Get the scanner instance
  		scanner = new Scanner(new File(imeDatoteke));
  		
  		scanner.useDelimiter("\n"); //razdvajamo redove
  		scanner.next(); //preskacemo prvi red
  		while(scanner.hasNext() && l<brojDatoteka){ //svaki red zapisuemo u polje line
  			line[l] = scanner.next();
  			l++;
  		}
  		
  		//line = scanner.next();	   			
  		//metrics = line.split(",");
	  } 
	  catch (FileNotFoundException fe) 
	  {
	  	fe.printStackTrace();
	  }
	  finally
	  {
	  	scanner.close();
	  }
	  for(int i = 0; i< brojDatoteka; i++){
		  //System.out.println(i);
		  metrics[i] = line[i].split(","); // svaki red razdvajamo po zarezu i spremamo u metrics
	  }
	  //metrics[i][0] je naziv datoteke
		  
	  return metrics;
  }
  
  public static void addY(double value){
	  if(indexY<y.length){
		  y[indexY] = value;
		  indexY++;
	  }
	  else System.out.println("nemoze");
  }
  
  public static void addY2(double value){
	  if(indexY2<y2.length){
		  y2[indexY2] = value;
		  indexY2++;
	  }
	  else System.out.println("nemoze");

  }
  
  public static void addY_2(double value){
	  if(indexY_2<y_2.length){
		  y_2[indexY_2] = value;
		  indexY_2++;
	  }
	  else System.out.println("nemoze");
  }
  
  public static void addY2_2(double value){
	  if(indexY2_2<y2_2.length){
		  y2_2[indexY2_2] = value;
		  indexY2_2++;
	  }
	  else System.out.println("nemoze");

  }
  
  public static void addYTest(double value){
	  if(indexYTest<yTest.length){
		  yTest[indexYTest] = value;
		  indexYTest++;
	  }
	  else System.out.println("nemoze");
  }
  
  public static void addY2Test(double value){
	  if(indexY2Test<y2Test.length){
		  y2Test[indexY2Test] = value;
		  indexY2Test++;
	  }
	  else System.out.println("nemoze");

  }
  
  
  public static void main(String [] args) throws 
                                  JMException, 
                                  SecurityException, 
                                  IOException, 
                                  ClassNotFoundException, InterruptedException {
    Problem problem   ; // The problem to solve
    Problem problem2;
    Problem kursawe;
    //Algorithm algorithm ; // The algorithm to use
    

    RitehAlgorithm algorithm, algorithm2;
    

    
    
    Operator  crossover ; // Crossover operator
    Operator  mutation  ; // Mutation operator
    Operator  selection ; // Selection operator
    
    HashMap  parameters ; // Operator parameters
    
    QualityIndicator indicators ; // Object to get quality indicators
    
    int brojDatoteka = 2397;
    boolean riteh=false;
    
  
	String[][] metrics = new String[brojDatoteka][49];
	
    // Logger object and file to store log messages
    logger_      = Configuration.logger_ ;
    fileHandler_ = new FileHandler("NSGAII_main.log"); 
    logger_.addHandler(fileHandler_) ;
        
    indicators = null ;
    if (args.length == 1) {
      Object [] params = {"Real"};
      problem = (new ProblemFactory()).getProblem(args[0],params);
      problem2 = (new ProblemFactory()).getProblem(args[0],params);
      kursawe =(new ProblemFactory()).getProblem(args[0],params);
    } // if
    else if (args.length == 2) {
      Object [] params = {"Real"};
      problem = (new ProblemFactory()).getProblem(args[0],params);
      problem2 = (new ProblemFactory()).getProblem(args[0],params);
      kursawe = (new ProblemFactory()).getProblem(args[0],params);
      indicators = new QualityIndicator(problem, args[1]) ;
    } // if
    else { // Default problem
      //problem = new Kursawe("Real", 3); 
      //problem = new Kursawe("BinaryReal", 3);
      //problem = new Water("Real");
      //problem = new ZDT1("ArrayReal", 100);
      //problem = new ConstrEx("Real");
      //problem = new DTLZ1("Real");
      //problem = new OKA2("Real") ;
      riteh=true; //ozna�imo da koristimo riteh problem, pozivamo ga kasnije jer nemamo jo� sve potrebne argumente
    } // else
    
    if (riteh){ //ako je riteh problem �itamo iz datoteke
    	
    	
    }
    metrics = readData(brojDatoteka, "JDT_R2_0.csv");
    problem = new RitehProblem("Real", metrics[0].length, metrics, brojDatoteka); 
    problem2 = new RitehProblem2("Real", metrics[0].length, metrics, brojDatoteka); 
	//kursawe = new Kursawe("Real", 3);

    
    //algorithm = new NSGAII(problem);
    //algorithm = new ssNSGAII(problem);

    algorithm = new RitehNSGAII(problem);
    algorithm2 = new RitehNSGAII(problem2);
    
    
    // Algorithm parameters
    algorithm.setInputParameter("populationSize",100);
    algorithm.setInputParameter("maxEvaluations", evaluationNumber); 
    
    algorithm2.setInputParameter("populationSize",100);
    algorithm2.setInputParameter("maxEvaluations",evaluationNumber); 
   
 

    // Mutation and Crossover for Real codification 
    parameters = new HashMap() ;
    parameters.put("probability", 0.9) ;
    parameters.put("distributionIndex", 20.0) ;
    crossover = CrossoverFactory.getCrossoverOperator("SBXCrossover", parameters);                   

    parameters = new HashMap() ;
    parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
    parameters.put("distributionIndex", 20.0) ;
    mutation = MutationFactory.getMutationOperator("PolynomialMutation", parameters);                    

    // Selection Operator 
    parameters = null ;
    selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters) ;                           

    // Add the operators to the algorithm
    algorithm.addOperator("crossover",crossover);
    algorithm.addOperator("mutation",mutation);
    algorithm.addOperator("selection",selection);
    
    algorithm2.addOperator("crossover",crossover);
    algorithm2.addOperator("mutation",mutation);
    algorithm2.addOperator("selection",selection);
    

    // Add the indicator object to the algorithm
    algorithm.setInputParameter("indicators", indicators) ;
    
    algorithm2.setInputParameter("indicators", indicators) ;
       
    // Execute the Algorithm
    long initTime = System.currentTimeMillis();
        
    SolutionSet population = algorithm.execute(1);
    System.out.println("Dobivena 1. populacija");
    
    SolutionSet population2 = algorithm2.execute(2);
    System.out.println("Dobivena 2. populacija");
       
    
    System.out.println("Kri�anje gena 1");
    //SolutionSet newPopulation1 = algorithm.combine(population, population2, 30); //od populacija koja je navedena prva, prvi dio gena ostaje originalan
    																			//veli�ina prvog i drugog dijela ovisi o postotku (zadnji argument)
    
    SolutionSet newPopulation1 = population;
    SolutionSet newPopulation2 = population2;
    System.out.println("Kri�anje gena 2");
    //SolutionSet newPopulation2 = algorithm.combine(population2, population, 30); //od populacija koja je navedena prva, prvi dio gena ostaje originalan
    																			//veli�ina prvog i drugog dijela ovisi o postotlu (zadnji argument)
    for(int i = 0; i<replaceNumber; i++){
	    newPopulation1 = algorithm.execute(newPopulation1,1);
	    System.out.println("Dobivena nova prva populacija");
	    
	    newPopulation2 = algorithm2.execute(newPopulation2,2);
	    System.out.println("Dobivena nova druga populacija");
	    
	    System.out.println("Zamijena jedinki");
	    newPopulation1 = algorithm.replace(newPopulation1, newPopulation2, 0); //u populaciji koja je prvi argument zamjenjujemo najgore
	    newPopulation2 = algorithm2.replace(newPopulation2, newPopulation1, 0); //s najboljima iz populacije koja je 2. argument
	    System.out.println("Zavr�ena zamijena jedinki");
    }

    newPopulation1 = algorithm.execute(newPopulation1,1);
    System.out.println("Dobivena prva kona�na gena");

    newPopulation2 = algorithm2.execute(newPopulation2,2);
    System.out.println("Dobivena druga kona�na gena");
    


    long estimatedTime = System.currentTimeMillis() - initTime;
        
    
    if(evaluationNumber % 200 !=0){
	    double [] pop = new double [100];
		for(int i = 0; i<100; i++){
			pop[i]=newPopulation1.get(i).getObjective(0);
		}
		Arrays.sort(pop);
		double median;
		if (pop.length % 2 == 0)
		    median = ((double)pop[pop.length/2] + (double)pop[pop.length/2 - 1])/2;
		else
		    median = (double) pop[pop.length/2];
	
		
		double [] pop2 = new double [100];
		for(int i = 0; i<100; i++){
			pop2[i]=newPopulation2.get(i).getObjective(0);
		}
		Arrays.sort(pop2);
		double median2;
		if (pop2.length % 2 == 0)
		    median2 = ((double)pop2[pop2.length/2] + (double)pop2[pop2.length/2 - 1])/2;
		else
		    median2 = (double) pop2[pop2.length/2];
		
		 double [] pop_2 = new double [100];
			for(int i = 0; i<100; i++){
				pop_2[i]=newPopulation1.get(i).getObjective(1);
			}
			Arrays.sort(pop_2);
			double median_2;
			if (pop_2.length % 2 == 0)
			    median_2 = ((double)pop_2[pop_2.length/2] + (double)pop_2[pop_2.length/2 - 1])/2;
			else
			    median_2 = (double) pop_2[pop_2.length/2];
	
			
			double [] pop2_2 = new double [100];
			for(int i = 0; i<100; i++){
				pop2_2[i]=newPopulation2.get(i).getObjective(1);
			}
			Arrays.sort(pop2_2);
			double median2_2;
			if (pop2_2.length % 2 == 0)
			    median2_2 = ((double)pop2_2[pop2_2.length/2] + (double)pop2_2[pop2_2.length/2 - 1])/2;
			else
			    median2_2 = (double) pop2_2[pop2_2.length/2];
		
		NSGAII_main.addY(1-median);
		NSGAII_main.addY2(1-median2);
		NSGAII_main.addY_2(1-median_2);
		NSGAII_main.addY2_2(1-median2_2);
    }
	
    
    // Result messages 
    logger_.info("Total execution time: "+estimatedTime + "ms");
    logger_.info("Variables values have been writen to file VAR");
   
    
    population.printVariablesToFile("VARpop1_12500");     //populacija prije kri�anja
    population2.printVariablesToFile("VARpop2_12500");     //populacija prije kri�anja

    newPopulation1.printVariablesToFile("VAR1");    //prva kona�na populacija
    newPopulation2.printVariablesToFile("VAR2");    //druga kona�na populacija
    
    logger_.info("Objectives values have been writen to file FUN");
    //population.printObjectivesToFile("FUNpop1_12500");     //populacija prije kri�anja
    //population2.printObjectivesToFile("FUNpop2_12500");     //populacija prije kri�anja
 
    newPopulation1.printObjectivesToFile("FUN1");	//prva konacna populacija
    newPopulation2.printObjectivesToFile("FUN2");	//druga kona�na populacija
    
    if (indicators != null) {
      logger_.info("Quality indicators") ;
      logger_.info("Hypervolume: " + indicators.getHypervolume(population)) ;
      logger_.info("GD         : " + indicators.getGD(population)) ;
      logger_.info("IGD        : " + indicators.getIGD(population)) ;
      logger_.info("Spread     : " + indicators.getSpread(population)) ;
      logger_.info("Epsilon    : " + indicators.getEpsilon(population)) ;  
     
      int evaluations = ((Integer)algorithm.getOutputParameter("evaluations")).intValue();
      logger_.info("Speed      : " + evaluations + " evaluations") ;     
    } // if
    
    
    Ranking ranking1 = new Ranking (newPopulation1);
    Ranking ranking2 = new Ranking (newPopulation2);
    
    ranking1.getSubfront(0).printObjectivesToFile("RANK1");
    ranking2.getSubfront(0).printObjectivesToFile("RANK2");
    
    double firstFF1[] = new double[ranking1.getSubfront(0).size()];
    double secondFF1[] = new double[ranking1.getSubfront(0).size()];
    double firstFF2[] = new double[ranking2.getSubfront(0).size()];
    double secondFF2[] = new double[ranking2.getSubfront(0).size()];
    
    double maxFirstFF1=0, maxSecondFF1=0, maxFirstFF2=0, maxSecondFF2=0;
    
    //pospremanje vrijednosti iz ranking u polja
    for(int i = 0; i < ranking1.getSubfront(0).size(); i++){
    	firstFF1[i] = 1-ranking1.getSubfront(0).get(i).getObjective(0);
    	if(firstFF1[i]>maxFirstFF1) maxFirstFF1= Math.ceil(firstFF1[i]*10);
    	secondFF1[i] = 1-ranking1.getSubfront(0).get(i).getObjective(1);
    	if(secondFF1[i]>maxSecondFF1) maxSecondFF1 = secondFF1[i];

    }
    
    for(int i = 0; i < ranking2.getSubfront(0).size(); i++){
    	firstFF2[i] = 1-ranking2.getSubfront(0).get(i).getObjective(0);
    	if(firstFF2[i]>maxFirstFF2) maxFirstFF2 = Math.ceil(firstFF2[i]*10);
    	secondFF2[i] = 1- ranking2.getSubfront(0).get(i).getObjective(1);
    	if(secondFF2[i]>maxSecondFF2) maxSecondFF2 =secondFF2[i];
    }
    
    //pronala�enje najve�e vrijednosti 2. FF u intervalima od 0.1 1.FF
    int max1[] = new int [10]; //index vrijednosti 2.FF za interval i
    Arrays.fill(max1, 0);
    for(int i = 0; i<firstFF1.length; i++){
    	if(firstFF1[i]>=0 && firstFF1[i]<=0.1 && secondFF1[i]>secondFF1[max1[0]]) max1[0] = i;  //ako se nalazi u prvom intervalu (1.FF) i ako je 2. FF najve�a 
    	else if(firstFF1[i]>0.1 && firstFF1[i]<=0.2 && secondFF1[i]>secondFF1[max1[1]]) max1[1] = i; //od ovih do sada postavi da je max
    	else if(firstFF1[i]>0.2 && firstFF1[i]<=0.3 && secondFF1[i]>secondFF1[max1[2]]) max1[2] = i;
    	else if(firstFF1[i]>0.3 && firstFF1[i]<=0.4 && secondFF1[i]>secondFF1[max1[3]]) max1[3] = i;
    	else if(firstFF1[i]>0.4 && firstFF1[i]<=0.5 && secondFF1[i]>secondFF1[max1[4]]) max1[4] = i;
    	else if(firstFF1[i]>0.5 && firstFF1[i]<=0.6 && secondFF1[i]>secondFF1[max1[5]]) max1[5] = i;
    	else if(firstFF1[i]>0.6 && firstFF1[i]<=0.7 && secondFF1[i]>secondFF1[max1[6]]) max1[6] = i;
    	else if(firstFF1[i]>0.7 && firstFF1[i]<=0.8 && secondFF1[i]>secondFF1[max1[7]]) max1[7] = i;
    	else if(firstFF1[i]>0.8 && firstFF1[i]<=0.9 && secondFF1[i]>secondFF1[max1[8]]) max1[8] = i;
    	else if(firstFF1[i]>0.9 && firstFF1[i]<=1 && secondFF1[i]>secondFF1[max1[9]]) max1[9] = i;
    }
    
    
    int max2[] = new int [10]; //index vrijednosti 2.FF za interval i
    Arrays.fill(max2, 0);
    for(int i = 0; i<firstFF2.length; i++){
    	if(firstFF2[i]>=0 && firstFF2[i]<=0.1 && secondFF2[i]>secondFF2[max2[0]]) max2[0] = i;  //ako se nalazi u prvom intervalu (1.FF) i ako je 2. FF najve�a 
    	else if(firstFF2[i]>0.1 && firstFF2[i]<=0.2 && secondFF2[i]>secondFF2[max2[1]]) max2[1] = i; //od ovih do sada postavi da je max
    	else if(firstFF2[i]>0.2 && firstFF2[i]<=0.3 && secondFF2[i]>secondFF2[max2[2]]) max2[2] = i;
    	else if(firstFF2[i]>0.3 && firstFF2[i]<=0.4 && secondFF2[i]>secondFF2[max2[3]]) max2[3] = i;
    	else if(firstFF2[i]>0.4 && firstFF2[i]<=0.5 && secondFF2[i]>secondFF2[max2[4]]) max2[4] = i;
    	else if(firstFF2[i]>0.5 && firstFF2[i]<=0.6 && secondFF2[i]>secondFF2[max2[5]]) max2[5] = i;
    	else if(firstFF2[i]>0.6 && firstFF2[i]<=0.7 && secondFF2[i]>secondFF2[max2[6]]) max2[6] = i;
    	else if(firstFF2[i]>0.7 && firstFF2[i]<=0.8 && secondFF2[i]>secondFF2[max2[7]]) max2[7] = i;
    	else if(firstFF2[i]>0.8 && firstFF2[i]<=0.9 && secondFF2[i]>secondFF2[max2[8]]) max2[8] = i;
    	else if(firstFF2[i]>0.9 && firstFF2[i]<=1 && secondFF2[i]>secondFF2[max2[9]]) max2[9] = i;
    }
    
    double paretoSurface1=0;
    double paretoSurface2=0;
    
    double[] paretoGraphData = new double [(int)maxFirstFF1];
    paretoGraphData[0] = 1;
    paretoGraphData[(int)maxFirstFF1-1] = 0;

    double[] X = new double [(int)maxFirstFF1];
	System.out.println(X.length);
    int j = 0;
    for (double i = 0.1; j < X.length; i= i + 0.1)  {
    	X[j] = i;
    	j++;
    }
    
    double[] rez = new double[(int)maxFirstFF1];
    double suma = 0;
    
    for(int i = 1; i<paretoGraphData.length-1; i++){
    	paretoGraphData[i] = secondFF1[max1[i-1]];
    	rez[i] = (paretoGraphData[i-1] + paretoGraphData[i]) / 2 * (X[i] - X[i-1]);
    	suma = suma + rez[i];
    }
       
    
    double[] rez2 = new double[(int)maxFirstFF1];
    double suma2 = 0;
    
    double[] paretoGraphData2 = new double [(int)maxFirstFF2];
    paretoGraphData2[0] = maxSecondFF2;
    paretoGraphData2[(int)maxFirstFF2-1] = 0;
    
    for(int i = 1; i<paretoGraphData2.length-1; i++){
    	paretoGraphData2[i] = secondFF2[max2[i-1]];
    	
    	rez2[i] = (paretoGraphData2[i-1] + paretoGraphData2[i]) / 2 * (X[i] - X[i-1]);
    	suma2 = suma2 + rez2[i];
    }
    
    
    
	System.out.println("Povr�ina ispod prve krivulje Pareto fronta: " + suma);
	System.out.println("Povr�ina ispod druge krivulje Pareto fronta: " + suma2);

    
    
      ///////Grafovi ucenja
	  
	  
	  double xFun[] = new double [numData];
	  xFun[0]=200;
	  for(int i=1; i<numData; i++){
		  xFun[i]=xFun[i-1]+200;
	  }
	  
	  	  
	  double xPar1[] = new double [paretoGraphData.length];
	  xPar1[0] = 0;
	  for(int i = 1; i < xPar1.length; i++){
		  xPar1[i] = xPar1[i-1] + 0.1;
	  }
	  xPar1[xPar1.length-1] = 1;
	  
	  double xPar2[] = new double [paretoGraphData.length];
	  xPar2[0] = 0;
	  for(int i = 1; i < xPar2.length; i++){
		  xPar2[i] = xPar2[i-1] + 0.1;
	  }
	  

      Plot plot = new Plot(y, xFun, 2500, "Generations", "TPR");
      plot.setLayout(null);
      JFrame frame = new JFrame("TPR");
      frame.setBackground(Color.white);
       
      frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frame.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
      frame.getContentPane().add(plot);
      plot.updateUI();
      frame.setSize(600,600);
      frame.setVisible(true);
      
      Plot plot2 = new Plot(y2, xFun, 2500, "Generations", "TNR");
      plot2.setLayout(null);
      JFrame frame2 = new JFrame("TNR");
      frame2.setBackground(Color.white);
       
      frame2.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frame2.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
      frame2.getContentPane().add(plot2);
      plot2.updateUI();
      frame2.setSize(600,600);
      frame2.setVisible(true);
      
      Plot plot3 = new Plot(y_2, xFun, 2500, "Generations", "F-measure");
      plot.setLayout(null);
      JFrame frame3 = new JFrame("F-measure");
      frame3.setBackground(Color.white);
       
      frame3.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frame3.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
      frame3.getContentPane().add(plot3);
      plot3.updateUI();
      frame3.setSize(600,600);
      frame3.setVisible(true);
      
      Plot plot4 = new Plot(y2_2, xFun, 2500, "Generations", "TPR-TNR average");
      plot4.setLayout(null);
      JFrame frame4 = new JFrame("TPR-TNR average");
      frame4.setBackground(Color.white);
       
      frame4.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frame4.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
      frame4.getContentPane().add(plot4);
      plot4.updateUI();
      frame4.setSize(600,600);
      frame4.setVisible(true);

      
      Plot plot5 = new Plot(paretoGraphData, xPar1, 0.2, "First Fitness Function - TPR", "Second Fitness Function - TNR");
      plot5.setLayout(null);
      JFrame frame5 = new JFrame("PARETO1");
      frame5.setBackground(Color.white);
       
      frame5.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frame5.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
      frame5.getContentPane().add(plot5);
      plot5.updateUI();
      frame5.setSize(900,600);
      frame5.setVisible(true);
      
      Plot plot6 = new Plot(paretoGraphData2, xPar2, 0.2, "First Fitness Function - PRECISION", "Second Fitness Function - ARITMETIC MEASURE");
      plot6.setLayout(null);
      JFrame frame6 = new JFrame("PARETO2");
      frame6.setBackground(Color.white);
       
      frame6.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frame6.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
      frame6.getContentPane().add(plot6);
      plot6.updateUI();
      frame6.setSize(900,600);
      frame6.setVisible(true);
      
      ////////////////////////////////////////////GRAFOVI TESTIRANJA REZULTATA S NOVIM PODACIMA
      
      int brojDatotekaTest = 2743;
      
      String [][] testMetrics = readData(brojDatotekaTest, "JDT_R2_1.csv");
      
      problem = new RitehProblem("Real", testMetrics[0].length, testMetrics, brojDatotekaTest); 
      
      for(int i = 0; i<replaceNumber+2; i++){
        newPopulation1 = algorithm.execute(newPopulation1,3);
      }
     /* 
      Plot plotTest = new Plot(yTest, xFun, 2500, "Generations", "TPR");
      plotTest.setLayout(null);
      JFrame frameTest = new JFrame("TPR");
      frameTest.setBackground(Color.white);
       
      frameTest.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frameTest.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
      frameTest.getContentPane().add(plotTest);
      plotTest.updateUI();
      frameTest.setSize(600,600);
      frameTest.setVisible(true);
      
      Plot plotTest2 = new Plot(y2Test, xFun, 2500, "Generations", "TNR");
      plotTest2.setLayout(null);
      JFrame frameTest2 = new JFrame("TNR");
      frameTest2.setBackground(Color.white);
       
      frameTest2.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frameTest2.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
      frameTest2.getContentPane().add(plotTest2);
      plotTest2.updateUI();
      frameTest2.setSize(600,600);
      frameTest2.setVisible(true);
*/
      
  } //main
} // NSGAII_main
      
      /////////////////////////////////////////////////////////////
      
