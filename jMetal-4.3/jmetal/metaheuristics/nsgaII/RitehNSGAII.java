//  RitehNSGAII.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.metaheuristics.nsgaII;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;

import jmetal.core.*;
import jmetal.util.comparators.CrowdingComparator;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.*;

/** 
 *  Implementation of NSGA-II.
 *  This implementation of NSGA-II makes use of a QualityIndicator object
 *  to obtained the convergence speed of the algorithm. This version is used
 *  in the paper:
 *     A.J. Nebro, J.J. Durillo, C.A. Coello Coello, F. Luna, E. Alba 
 *     "A Study of Convergence Speed in Multi-Objective Metaheuristics." 
 *     To be presented in: PPSN'08. Dortmund. September 2008.
 */

public class RitehNSGAII extends RitehAlgorithm {

	/**
	 * Constructor
	 * @param problem Problem to solve
	 */
	public RitehNSGAII(Problem problem) {
		super (problem) ;
	} // RitehNSGAII

	/**   
	 * Runs the NSGA-II algorithm.
	 * @return a <code>SolutionSet</code> that is a set of non dominated solutions
	 * as a result of the algorithm execution
	 * @throws JMException 
	 * @throws InterruptedException 
	 */
	public SolutionSet execute(int numPop) throws JMException, ClassNotFoundException, InterruptedException {
		int populationSize;
		int maxEvaluations;
		int evaluations;

		QualityIndicator indicators; // QualityIndicator object
		int requiredEvaluations; // Use in the example of use of the
		// indicators object (see below)

		SolutionSet population;
		SolutionSet offspringPopulation;
		SolutionSet union;

		Operator mutationOperator;
		Operator crossoverOperator;
		Operator selectionOperator;

		Distance distance = new Distance();

		//Read the parameters
		populationSize = ((Integer) getInputParameter("populationSize")).intValue();
		maxEvaluations = ((Integer) getInputParameter("maxEvaluations")).intValue();
		indicators = (QualityIndicator) getInputParameter("indicators");

		//Initialize the variables
		population = new SolutionSet(populationSize);
		evaluations = 0;

		requiredEvaluations = 0;

		//Read the operators
		mutationOperator = operators_.get("mutation");
		crossoverOperator = operators_.get("crossover");
		selectionOperator = operators_.get("selection");

		// Create the initial solutionSet
		Solution newSolution;
		for (int i = 0; i < populationSize; i++) {
			newSolution = new Solution(problem_);
			problem_.evaluate(newSolution);
			problem_.evaluateConstraints(newSolution);
			evaluations++;
			population.add(newSolution);
		} //for       
	


		// Generations 
		while (evaluations < maxEvaluations) {

			// Create the offSpring solutionSet      
			offspringPopulation = new SolutionSet(populationSize);
			Solution[] parents = new Solution[2];
			for (int i = 0; i < (populationSize / 2); i++) {
				if (evaluations < maxEvaluations) {
					//obtain parents
					parents[0] = (Solution) selectionOperator.execute(population);
					parents[1] = (Solution) selectionOperator.execute(population);
					Solution[] offSpring = (Solution[]) crossoverOperator.execute(parents);
					mutationOperator.execute(offSpring[0]);
					mutationOperator.execute(offSpring[1]);
					problem_.evaluate(offSpring[0]);
					problem_.evaluateConstraints(offSpring[0]);
					problem_.evaluate(offSpring[1]);
					problem_.evaluateConstraints(offSpring[1]);
					offspringPopulation.add(offSpring[0]);
					offspringPopulation.add(offSpring[1]);
					evaluations += 2;
					
				} // if                            
			} // for

			// Create the solutionSet union of solutionSet and offSpring
			union = ((SolutionSet) population).union(offspringPopulation);

			// Ranking the union
			Ranking ranking = new Ranking(union);

			
			int remain = populationSize;
			
			int index = 0;
			SolutionSet front = null;
			population.clear();

			// Obtain the next front
			front = ranking.getSubfront(index);

			while ((remain > 0) && (remain >= front.size())) {
				//Assign crowding distance to individuals
				distance.crowdingDistanceAssignment(front, problem_.getNumberOfObjectives());
				//Add the individuals of this front
				for (int k = 0; k < front.size(); k++) {
					population.add(front.get(k));
				} // for

				//Decrement remain
				remain = remain - front.size();

				//Obtain the next front
				index++;
				if (remain > 0) {
					front = ranking.getSubfront(index);
				} // if        
			} // while

			// Remain is less than front(index).size, insert only the best one
			if (remain > 0) {  // front contains individuals to insert                        
				distance.crowdingDistanceAssignment(front, problem_.getNumberOfObjectives());
				front.sort(new CrowdingComparator());
				for (int k = 0; k < remain; k++) {
					population.add(front.get(k));
				} // for

				remain = 0;
			} // if                               

			// This piece of code shows how to use the indicator object into the code
			// of NSGA-II. In particular, it finds the number of evaluations required
			// by the algorithm to obtain a Pareto front with a hypervolume higher
			// than the hypervolume of the true Pareto front.
			if ((indicators != null) &&
					(requiredEvaluations == 0)) {
				double HV = indicators.getHypervolume(population);
				if (HV >= (0.98 * indicators.getTrueParetoFrontHypervolume())) {
					requiredEvaluations = evaluations;
				} // if
			} // if-
			
			double [] pop = new double [populationSize];
			for(int i = 0; i<populationSize; i++){
				pop[i]=population.get(i).getObjective(0);
			}
			Arrays.sort(pop);
			double median;
			if (pop.length % 2 == 0)
			    median = ((double)pop[pop.length/2] + (double)pop[pop.length/2 - 1])/2;
			else
			    median = (double) pop[pop.length/2];
		
			
			double [] pop2 = new double [populationSize];
			for(int i = 0; i<populationSize; i++){
				pop2[i]=population.get(i).getObjective(1);
			}
			Arrays.sort(pop2);
			double median2;
			if (pop2.length % 2 == 0)
			    median2 = ((double)pop2[pop2.length/2] + (double)pop2[pop2.length/2 - 1])/2;
			else
			    median2 = (double) pop2[pop2.length/2];
			
			if(evaluations%200 == 0 && numPop == 1) NSGAII_main.addY(1-median);
			if(evaluations%200 == 0 && numPop == 2) NSGAII_main.addY2(1-median);
			if(evaluations%200 == 0 && numPop == 3) NSGAII_main.addYTest(1-median);

			
			if(evaluations%200 == 0 && numPop == 1) NSGAII_main.addY_2(1-median2);
			if(evaluations%200 == 0 && numPop == 2) NSGAII_main.addY2_2(1-median2);
			if(evaluations%200 == 0 && numPop == 3) NSGAII_main.addY2Test(1-median2);

	

//			if(evaluations == 2500 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_2500", prosjek);
//			if(evaluations == 5000 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_5000", prosjek);
//			if(evaluations == 7500 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_7500", prosjek);
//			if(evaluations == 10000 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_10000", prosjek);
//			if(evaluations == 12500 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_12500", prosjek);

		} // while

		// Return as output parameter the required evaluations
		setOutputParameter("evaluations", requiredEvaluations);
		//System.out.println(population.size());

		// Return the first non-dominated front
		population.printObjectivesToFile("FUN"); //ispisivanje svih rezultata zadnje populacije u file
		population.printVariablesToFile("VAR"); //ispisivanje svih varijabli zadnje populacije u file
		Ranking ranking = new Ranking(population);

	
		//return ranking.getSubfront(0);
		return population; //kako bi dobili cijelu populaciju a ne samo najbolja rje�enja
	} // execute
	
	public SolutionSet execute(SolutionSet initialPopulation, int numPop) throws JMException, ClassNotFoundException, InterruptedException {
		
		
		int populationSize;
		int maxEvaluations;
		int evaluations;

		QualityIndicator indicators; // QualityIndicator object
		int requiredEvaluations; // Use in the example of use of the
		// indicators object (see below)

		SolutionSet population;
		SolutionSet offspringPopulation;
		SolutionSet union;

		Operator mutationOperator;
		Operator crossoverOperator;
		Operator selectionOperator;

		Distance distance = new Distance();

		//Read the parameters
		populationSize = ((Integer) getInputParameter("populationSize")).intValue();
		maxEvaluations = ((Integer) getInputParameter("maxEvaluations")).intValue();
		indicators = (QualityIndicator) getInputParameter("indicators");
		
		population = initialPopulation;
	

		evaluations = 0;

		requiredEvaluations = 0;

		//Read the operators
		mutationOperator = operators_.get("mutation");
		crossoverOperator = operators_.get("crossover");
		selectionOperator = operators_.get("selection");

		while (evaluations < maxEvaluations) {

			// Create the offSpring solutionSet      
			offspringPopulation = new SolutionSet(populationSize);
			Solution[] parents = new Solution[2];
			for (int i = 0; i < (populationSize / 2); i++) {
				if (evaluations < maxEvaluations) {
					//obtain parents
					parents[0] = (Solution) selectionOperator.execute(population);
					parents[1] = (Solution) selectionOperator.execute(population);
					Solution[] offSpring = (Solution[]) crossoverOperator.execute(parents);
					mutationOperator.execute(offSpring[0]);
					mutationOperator.execute(offSpring[1]);
					problem_.evaluate(offSpring[0]);
					problem_.evaluateConstraints(offSpring[0]);
					problem_.evaluate(offSpring[1]);
					problem_.evaluateConstraints(offSpring[1]);
					offspringPopulation.add(offSpring[0]);
					offspringPopulation.add(offSpring[1]);
					evaluations += 2;
					
				} // if         
			} // for

			// Create the solutionSet union of solutionSet and offSpring
			union = ((SolutionSet) population).union(offspringPopulation);

			// Ranking the union
			Ranking ranking = new Ranking(union);
			int remain = populationSize;
			
			int index = 0;
			SolutionSet front = null;
			population.clear();

			// Obtain the next front
			front = ranking.getSubfront(index);

			while ((remain > 0) && (remain >= front.size())) {
				//Assign crowding distance to individuals
				distance.crowdingDistanceAssignment(front, problem_.getNumberOfObjectives());
				//Add the individuals of this front
				for (int k = 0; k < front.size(); k++) {
					population.add(front.get(k));
				} // for

				//Decrement remain
				remain = remain - front.size();

				//Obtain the next front
				index++;
				if (remain > 0) {
					front = ranking.getSubfront(index);
				} // if        
			} // while

			// Remain is less than front(index).size, insert only the best one
			if (remain > 0) {  // front contains individuals to insert                        
				distance.crowdingDistanceAssignment(front, problem_.getNumberOfObjectives());
				front.sort(new CrowdingComparator());
				for (int k = 0; k < remain; k++) {
					population.add(front.get(k));
				} // for

				remain = 0;
			} // if                               

			// This piece of code shows how to use the indicator object into the code
			// of NSGA-II. In particular, it finds the number of evaluations required
			// by the algorithm to obtain a Pareto front with a hypervolume higher
			// than the hypervolume of the true Pareto front.
			if ((indicators != null) &&
					(requiredEvaluations == 0)) {
				double HV = indicators.getHypervolume(population);
				if (HV >= (0.98 * indicators.getTrueParetoFrontHypervolume())) {
					requiredEvaluations = evaluations;
				} // if
			} // if
			
			double [] pop = new double [populationSize];
			for(int i = 0; i<populationSize; i++){
				pop[i]=population.get(i).getObjective(0);
			}
			Arrays.sort(pop);
			double median;
			if (pop.length % 2 == 0)
			    median = ((double)pop[pop.length/2] + (double)pop[pop.length/2 - 1])/2;
			else
			    median = (double) pop[pop.length/2];
		

			double [] pop2 = new double [populationSize];
			for(int i = 0; i<populationSize; i++){
				pop2[i]=population.get(i).getObjective(1);
			}
			Arrays.sort(pop2);
			double median2;
			if (pop2.length % 2 == 0)
			    median2 = ((double)pop2[pop2.length/2] + (double)pop2[pop2.length/2 - 1])/2;
			else
			    median2 = (double) pop2[pop2.length/2];
			
			

			
			if(evaluations%200 == 0 && numPop == 1) NSGAII_main.addY(1-median);
			if(evaluations%200 == 0 && numPop == 2) NSGAII_main.addY2(1-median);
			if(evaluations%200 == 0 && numPop == 3) NSGAII_main.addYTest(1-median);

			
			if(evaluations%200 == 0 && numPop == 1) NSGAII_main.addY_2(1-median2);
			if(evaluations%200 == 0 && numPop == 2) NSGAII_main.addY2_2(1-median2);
			if(evaluations%200 == 0 && numPop == 3) NSGAII_main.addY2Test(1-median2);

	

//			if(evaluations == 2500 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_15000", prosjek);
//			if(evaluations == 5000 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_17500", prosjek);
//			if(evaluations == 7500 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_20000", prosjek);
//			if(evaluations == 10000 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_22500", prosjek);
//			if(evaluations == 12500 && numPop == 2) RitehNSGAII.printToFile("FUNpop2_25000", prosjek);
//			
//			if(evaluations == 2500 && numPop == 3) RitehNSGAII.printToFile("FUNpop3_15000", prosjek);
//			if(evaluations == 5000 && numPop == 3) RitehNSGAII.printToFile("FUNpop3_17500", prosjek);
//			if(evaluations == 7500 && numPop == 3) RitehNSGAII.printToFile("FUNpop3_20000", prosjek);
//			if(evaluations == 10000 && numPop == 3) RitehNSGAII.printToFile("FUNpop3_22500", prosjek);
//			if(evaluations == 12500 && numPop == 3) RitehNSGAII.printToFile("FUNpop3_25000", prosjek);
//	
//
//			if(evaluations == 2500 && numPop == 4) RitehNSGAII.printToFile("FUNpop4_15000", prosjek);
//			if(evaluations == 5000 && numPop == 4) RitehNSGAII.printToFile("FUNpop4_17500", prosjek);
//			if(evaluations == 7500 && numPop == 4) RitehNSGAII.printToFile("FUNpop4_20000", prosjek);
//			if(evaluations == 10000 && numPop == 4) RitehNSGAII.printToFile("FUNpop4_22500", prosjek);
//			if(evaluations == 12500 && numPop == 4) RitehNSGAII.printToFile("FUNpop4_25000", prosjek);


		} // while

		// Return as output parameter the required evaluations
		setOutputParameter("evaluations", requiredEvaluations);
		//System.out.println(population.size());

		// Return the first non-dominated front
		
		//Ranking ranking = new Ranking(population);
		//return ranking.getSubfront(0);
		
		return population;
		
	} //execute2
	
	
	public SolutionSet combine(SolutionSet population1, SolutionSet population2, double percentage) throws JMException, ClassNotFoundException{
		//od populacija koja je navedena prva, prvi dio gena ostaje originalan
		//veli�ina prvog i drugog dijela ovisi o postotku (zadnji argument)
		SolutionSet combinedPopulation = new SolutionSet(population1.getMaxSize());
				
		Solution []newSolution = new Solution [population1.getMaxSize()];
		int numberOfVar=population1.get(0).numberOfVariables();
		
		
	
		for(int i = 0; i<population1.size(); i++){
			for(int j=0; j<numberOfVar; j++){
				newSolution[i] = new Solution(population1.get(i));
				if(j<numberOfVar/(100/percentage)) newSolution[i].getDecisionVariables()[j].setValue(population1.get(i).getDecisionVariables()[j].getValue());
				else newSolution[i].getDecisionVariables()[j].setValue(population2.get(i).getDecisionVariables()[j].getValue());	
			}
			combinedPopulation.add(newSolution[i]);
			
		}
		return combinedPopulation;
	}//combine
	
public SolutionSet replace (SolutionSet population1, SolutionSet population2, double percentage){
	//u populaciji koja je prvi argument zamjenjujemo najgore
	//s najboljima iz populacije koja je 2. argument
	
		int popSize = population1.size();
		int cnt=(int)(population1.size()*(percentage/100));
		int brojac =0;
		SolutionSet bestRank = new SolutionSet(cnt);
		Ranking rank2 = new Ranking(population2); 
		
		for(int i = 0; i<rank2.getNumberOfSubfronts(); i++){
			for(int j = 0; (j<rank2.getSubfront(i).size() && brojac<cnt); j++){
				bestRank.add(rank2.getSubfront(i).get(j));
				brojac++;
			}
		}
				
			
		for(int i=0; i<cnt; i++){
			population1.replace(popSize-1-cnt, bestRank.get(i));
		}
				
		return population1;
}//replace

	
public static void printToFile(String path, double value){
		    try {
		      /* Open the file */
		      FileOutputStream fos   = new FileOutputStream(path)     ;
		      OutputStreamWriter osw = new OutputStreamWriter(fos)    ;
		      BufferedWriter bw      = new BufferedWriter(osw)        ;
		      
		      System.out.print("");
		      
		      bw.write(Double.toString(value));
		    
		      /* Close the file */
		      bw.close();
		    }catch (IOException e) {
		      Configuration.logger_.severe("Error acceding to the file");
		      e.printStackTrace();
		    }
		  } // printToFile	
		
//		for(int i = 0; i<combinedPopulation.size(); i++){
//			for(int j = 0; j<numberOfVar; j++){
//				System.out.println(combinedPopulation.get(i).getDecisionVariables()[j].getValue());
//			}
//			System.out.println("-----------------------------");
//		}
		
		
	
} // NSGA-II
