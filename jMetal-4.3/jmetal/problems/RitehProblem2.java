package jmetal.problems;

import jmetal.core.Solution;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public class RitehProblem2 extends RitehProblem {

	public RitehProblem2(String solutionType, Integer numberOfVariables, String[][] metricsMatrix, int numDat)
			throws ClassNotFoundException {
		super(solutionType, numberOfVariables, metricsMatrix, numDat);
		// TODO Auto-generated constructor stub
	}
	
	public void evaluate(Solution solution) throws JMException {
		XReal vars = new XReal(solution) ;
	       
	    double [] fx = new double [brDat] ; // function values     
	    double [] x = new double[numberOfVariables_] ;

	    int [] bug_cnt = new int [brDat];
	    int [] ff = new int [brDat];
	    
	    double TruePositive=0;
	    double TrueNegative=0;
	    double FalseNegative=0;
	    double FalsePositive=0;
	    double brojac=0;
	    
	    for (int i = 0 ; i < numberOfVariables_; i++){
	    	x[i] = vars.getValue(i) ;
	    }
	    a++;

	    for(int i=0; i<brDat; i++){
	    	for(int j = 1; j<numberOfVariables_; j++){
	            if(j!=16 || j!=34){
	            	fx[i] += x[j-1] * metrics[i][j];
	            }
	    	} 
	        //BUG_CNT
	    	
	        if(goal[i]==0) bug_cnt[i]=0;
	        else if (goal[i]>0) bug_cnt[i]=1;
	        
	        
	        //FX iz NSGA-II
	        if(fx[i]<0) ff[i]=0;
	        else ff[i]=1;


	        if(bug_cnt[i]==0 && ff[i]==0){ TrueNegative++;}
	        if(bug_cnt[i]==1 && ff[i]==1){ TruePositive++;}
	        if(bug_cnt[i]==1 && ff[i]==0){ FalseNegative++;}
	        if(bug_cnt[i]==0 && ff[i]==1){ FalsePositive++;}
	        
	        brojac++;
	       
	    }
	    double TPR = TruePositive/(TruePositive+FalseNegative);
	    double TNR = TrueNegative/(TrueNegative+FalsePositive);
	    
	    
	    double precision = TruePositive/(TruePositive + FalsePositive);
	    double recall = TruePositive/(TruePositive + FalseNegative);
	    
	    double F1 = 2 * ((precision*recall)/(precision+recall)); //prva ff
	    
	    double aritMean = (TPR + TNR) / 2;
	    
	    solution.setObjective(0, 1-F1); 
	    solution.setObjective(1, 1-aritMean); 
	    // druga FF 1.f-measire - f1 score 
	    // 2. aritm. sred. od (tpr + tnr) /2

	
	}

}
