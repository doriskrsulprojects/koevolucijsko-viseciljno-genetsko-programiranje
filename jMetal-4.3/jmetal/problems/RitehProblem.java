//  RitehProblem.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.problems;

import java.util.Arrays;

import jmetal.core.*;
import jmetal.encodings.solutionType.ArrayRealSolutionType;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;
import jmetal.util.Configuration.*;
import jmetal.util.wrapper.XReal;





/**
 * Class representing problem RitehProblem
 */
public class RitehProblem extends Problem {  
	int a = 0;
	
	public static double [] goal;
	public static double [][] metrics;
	public static int brDat;
    public int brojac =0;

  /** 
   * Constructor.
   * Creates a new instance of the RitehProblem problem.
   * @param numberOfVariables Number of variables of the problem 
   * @param solutionType The solution type must "Real", "BinaryReal, and "ArrayReal". 
   */
  public RitehProblem(String solutionType, Integer numberOfVariables, String [][] metricsMatrix, int numDat) throws ClassNotFoundException {
    numberOfVariables_   = numberOfVariables.intValue() ;
    numberOfObjectives_  = 2                            ;
    numberOfConstraints_ = 0                            ;
    problemName_         = "RitehProblem"               ;
    
    brDat = numDat;
    
    goal = new double[numDat];
    metrics = new double [numDat][numberOfVariables-2];
    
    for(int dat=0; dat<numDat; dat++){
    	for(int met =1; met<numberOfVariables; met++){
    		if(met!=numberOfVariables-1){
    			metrics[dat][met-1] = Double.parseDouble(metricsMatrix[dat][met]);
    		}
    		else goal[dat] = Double.parseDouble(metricsMatrix[dat][met]);
    		
    	}
    }
    
    
    
  
    numberOfVariables_ = numberOfVariables_-2;
    
    upperLimit_ = new double[numberOfVariables_] ;
    lowerLimit_ = new double[numberOfVariables_] ;
       
    for (int i = 0; i < numberOfVariables_; i++) {
      lowerLimit_[i] = -1000.0 ;
      upperLimit_[i] = 1000.0  ;
    } // for
        
    if (solutionType.compareTo("BinaryReal") == 0)
    	solutionType_ = new BinaryRealSolutionType(this) ;
    else if (solutionType.compareTo("Real") == 0)
    	solutionType_ = new RealSolutionType(this) ;
    else if (solutionType.compareTo("ArrayReal") == 0)
    	solutionType_ = new ArrayRealSolutionType(this) ;
    else {
    	System.out.println("Error: solution type " + solutionType + " invalid") ;
    	System.exit(-1) ;
    }
  } // RitehProblem
    
  /** 
  * Evaluates a solution 
  * @param solution The solution to evaluate
   * @throws JMException 
  */
  public void evaluate(Solution solution) throws JMException {
		XReal vars = new XReal(solution) ;
	       
	    double [] fx = new double [brDat] ; // function values     
	    double [] x = new double[numberOfVariables_] ;
	    //int TruePositive=0, TrueNegative = 0, FalsePositive=0, FalseNegative=0; 
	    int [] bug_cnt = new int [brDat];
	    int [] ff = new int [brDat];
	    
	    double TruePositive=0;
	    double TrueNegative=0;
	    double FalseNegative=0;
	    double FalsePositive=0;
	    
	    for (int i = 0 ; i < numberOfVariables_; i++){
	    	x[i] = vars.getValue(i) ;
	    }
	    a++;
	  
	    for(int i=0; i<brDat; i++){
	    	for(int j = 1; j<numberOfVariables_; j++){
	            if(j!=16 || j!=34){
	            	fx[i] += x[j-1] * metrics[i][j];
	            }
	    	} 
	        //BUG_CNT
	    	
	        if(goal[i]==0) bug_cnt[i]=0;
	        else if (goal[i]>0) bug_cnt[i]=1;
	        
	        
	        //FX iz NSGA-II
	        if(fx[i]<0) ff[i]=0;
	        else ff[i]=1;
	       
	        if(bug_cnt[i]==0 && ff[i]==0){ TrueNegative++;}
	        if(bug_cnt[i]==1 && ff[i]==1){ TruePositive++;}
	        if(bug_cnt[i]==1 && ff[i]==0){ FalseNegative++;}
	        if(bug_cnt[i]==0 && ff[i]==1){ FalsePositive++;}
	        
	        brojac++;
	       
	    }
	    double TPR = TruePositive/(TruePositive+FalseNegative);
	    double TNR = TrueNegative/(TrueNegative+FalsePositive);

	    solution.setObjective(0, (1-TPR)); 
	    solution.setObjective(1, (1-TNR)); 
	    // druga FF 1.f-measire - f1 score (ne beta nego 2
	    // 2. aritm. sred. od (tpr + tnr) /2
	    
  } // evaluate
} // RitehProblem
